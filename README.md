# Text summarizer

* Create a conda environment through the environment.yml file. Note: The environment was created on Linux and may not work in Windows environment. Manually build the packages listed in environment.yml file for windows.
* Activate the environment.
* Run the gui.py file for gui or histogram.py, rake_tokenizer.py, sumy_summarizer.py individually for command line input and output.